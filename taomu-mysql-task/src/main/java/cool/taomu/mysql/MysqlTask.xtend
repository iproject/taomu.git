package cool.taomu.mysql

import com.google.auto.service.AutoService
import com.mchange.v2.c3p0.ComboPooledDataSource
import cool.taomu.core.inter.ITaskQueue
import cool.taomu.core.inter.Callback
import cool.taomu.core.TaomuRunnable

@AutoService(ITaskQueue)
class MysqlTask implements ITaskQueue {
	ComboPooledDataSource dataSource;

	override void init() {
		var path = System.getProperty("user.dir");
		println(path);
		System.setProperty("com.mchange.v2.c3p0.cfg.xml",path+"/config/c3p0-config.xml")
		dataSource = new ComboPooledDataSource("stock_task");
	}
	
	override poll() {
	}
	
	override add(Runnable e) {
		return true;
	}
	
	
	override trigger(Callback<TaomuRunnable> r) {
		return null;
	}

}
