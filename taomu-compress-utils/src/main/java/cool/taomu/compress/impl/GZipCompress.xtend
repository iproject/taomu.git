/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.compress.impl

import cool.taomu.compress.inter.ICompress
import java.io.InputStream
import java.io.OutputStream
import java.util.zip.GZIPInputStream
import java.util.zip.GZIPOutputStream

class GZipCompress implements ICompress {
	ICompress ics;
	OutputStream output;
	InputStream input ;
	new(){}
	new(ICompress ics) {
		this.ics = ics;
	}

	override compression(OutputStream out) {
		if (ics !== null) {
			this.output = new GZIPOutputStream(ics.compression(out));
			return this.output;
		} else {
			this.output = new GZIPOutputStream(out);
			return this.output;
		}
	}

	override decompression(InputStream input) {
		this.input = new GZIPInputStream(input);
		if(this.ics !== null){
			return ics.decompression(this.input);
		}else{
			return this.input;
		}
	}

	override close() throws Exception {
		if(this.output !== null){
			this.output.close();
		}
		if(this.input !== null){
			this.input.close();
		}
		if (ics !== null) {
			this.ics.close();
		}
	}

}
