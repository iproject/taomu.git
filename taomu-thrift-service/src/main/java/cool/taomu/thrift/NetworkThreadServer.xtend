/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.thrift

import com.google.auto.service.AutoService
import cool.taomu.core.TaomuTask
import cool.taomu.core.entity.TaskEntity
import cool.taomu.core.inter.ITaomuService
import cool.taomu.thrift.entity.ThriftConfig
import cool.taomu.util.ThriftUtils
import cool.taomu.util.YamlUtils
import java.net.InetSocketAddress
import java.nio.ByteBuffer
import java.util.Arrays
import org.apache.commons.lang3.SerializationException
import org.apache.commons.lang3.SerializationUtils
import org.apache.thrift.TException
import org.apache.thrift.TProcessor
import org.slf4j.LoggerFactory

@AutoService(ITaomuService)
class NetworkThreadServer implements NetworkThread.Iface, ITaomuService {
	val LOG = LoggerFactory.getLogger(NetworkThreadServer);
	var static TaomuTask taomuTask = TaomuTask.instance;

	override execute(ByteBuffer data) throws TException {
		try {
			var ser = Arrays.copyOfRange(data.array(), data.position(), data.limit());
			var task = SerializationUtils.deserialize(ser) as TaskEntity;
			if (taomuTask !== null) {
				taomuTask.add(task);
			}else{
				throw new NullPointerException();
			}
		} catch (SerializationException ex) {
			LOG.info("反序列化失败", ex);
		} catch (Exception ex) {
			LOG.info("Exception:", ex);
		}
	}

	/*def getObjectInputStreamAware(InputStream bais, AbsWrapperClassLoader dest) {
	 * 	return new ObjectInputStreamAware(bais, dest);
	 }*/
	def static start(String host, int port, int max, int min) {
		var TProcessor tprocessor = new NetworkThread.Processor(new NetworkThreadServer());
		var isa = new InetSocketAddress(host, port);
		ThriftUtils.startServer(tprocessor, isa, max, min);
	}

	def static start(String host, int port) {
		var TProcessor tprocessor = new NetworkThread.Processor(new NetworkThreadServer());
		var isa = new InetSocketAddress(host, port);
		ThriftUtils.startServer(tprocessor, isa);
	}

	override service(TaomuTask factory) {
		taomuTask = factory;
		var config = YamlUtils.read("./config/thrift.yml", ThriftConfig);
		start(config.host, config.port, config.maxThreads, config.minThreads);
	}

}
