package cool.taomu.storage.sqlite

import cool.taomu.storage.JdbcUtils
import java.sql.Connection
import java.sql.DriverManager
import java.sql.SQLException
import java.util.List
import org.eclipse.xtend.lib.annotations.Accessors

@Accessors
class SqliteUtils {
	static final String DB_DRIVER = "org.sqlite.JDBC";
	static final String DB_CONNECTION = "jdbc:sqlite::memory:";

	def Connection getConnection() {
		var Connection connection = null;
		try {
			Class.forName(DB_DRIVER);
			connection = DriverManager.getConnection(DB_CONNECTION);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return connection;
	}


	def <T> List<T> select(String sql, Class<T> zlass, Object ... params) {
		try(var jdbc = new JdbcUtils(this.connection)){
			return jdbc.queryEntity(sql, zlass, params);
		}
	}

	def modify(String sql) {
		return this.modify(sql, null);
	}

	def modify(String sql, Object ... params) {
		try(var jdbc = new JdbcUtils(this.connection)){
			return jdbc.update(sql,params);
		}
	}
	
}
