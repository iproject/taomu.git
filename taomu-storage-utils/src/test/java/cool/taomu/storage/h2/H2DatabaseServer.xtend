/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package  cool.taomu.storage.h2

import java.sql.SQLException
import org.h2.tools.Server
import org.slf4j.LoggerFactory

class H2DatabaseServer {
	val static LOG = LoggerFactory.getLogger(H2DatabaseServer)

	def static startServer(Integer port) {
		try {
			// 8092
			LOG.info("启动H2服务器 port" + port)
			Server.createTcpServer(#["-tcpPort", String.valueOf(port)]).start()
		} catch (SQLException ex) {
			LOG.info(ex.message, ex);
		}
	}
}
