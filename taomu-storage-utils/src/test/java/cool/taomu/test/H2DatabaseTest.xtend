package cool.taomu.test

import cool.taomu.storage.h2.H2Utils
import org.eclipse.xtend.lib.annotations.Accessors
import org.eclipse.xtend.lib.annotations.ToString

class H2DatabaseTest {
	@Accessors
	@ToString
	static class Person{
		int id;	
		String name;
	}
	
	def static void main(String[] args) {
		var h2 = new H2Utils()
		println(h2.modify("CREATE TABLE PERSON(id int primary key, name varchar(255))"));
		h2.modify("INSERT INTO PERSON(id, name) VALUES(1, 'Anju')");
		h2.modify("INSERT INTO PERSON(id, name) VALUES(2, 'Hello World')");
		var rs  = h2.select("select * from PERSON",Person);
		println(rs);
	}
}
