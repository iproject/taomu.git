package cool.taomu.test.ssh

import org.junit.Test
import cool.taomu.ssh.JschUtils

class JschUtilsTest {
	
	@Test
	def void t1(){
		try(var jsch = new JschUtils("192.168.1.222","rcmu","mrc@2018",22)){
			try(var ssh = jsch.Ssh)	{
				println(ssh.exec("ls -l","UTF-8"));
			}
			try(var sftp = jsch.Sftp){
				println(sftp.get("/home/rcmu/ffmpeg_b.sh").size);
				println(sftp.get("/home/rcmu/ffmpeg_b.sh").size);
				println(sftp.get("/home/rcmu/ffmpeg_b.sh").size);
				println(sftp.get("/home/rcmu/ffmpeg_b.sh").size);
				println(sftp.get("/home/rcmu/ffmpeg_b.sh").size);
			}
		}
	}	
	
	@Test
	def void t2(){
		try(var jsch = new JschUtils("192.168.1.227","rcmu","mrc2018",22)){
			try(var s = jsch.Shell){
				println(s.exec(#["ls -l *","exit"]));
			}
		}
	}
}