/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.ssh

import com.jcraft.jsch.SftpProgressMonitor
import org.slf4j.LoggerFactory

class SftpProgresssMonitorImpl implements SftpProgressMonitor {
	val static LOG = LoggerFactory.getLogger(SftpProgresssMonitorImpl);
	long transfered = 0;
	long max = 0;
	long percent = -1;

	override count(long count) {
		this.transfered = this.transfered + count;
		if(this.percent >= this.transfered * 100 / this.max){
			return true;
		}
		this.percent = this.transfered * 100 / this.max;
		LOG.info("Completed {} ({}%) out of {}.",Long.valueOf(this.transfered),Long.valueOf(this.percent),Long.valueOf(this.max));
		return true;
	}

	override end() {
		LOG.info("done.");
	}

	override init(int op, String src, String dest, long max) {
		this.max = max;
		LOG.info("文件总大小:{}",max);
	}

}
