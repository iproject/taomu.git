package cool.taomu.test.mqttv3

import cool.taomu.mqtt.mqttv3.MqttCallback
import cool.taomu.mqtt.mqttv3.MqttV3Service
import cool.taomu.mqtt.mqttv3.Topic
import cool.taomu.mqtt.mqttv3.Topic.MessageType
import java.util.concurrent.Executors

class MqttV3Test {
	
	@Topic(value="test/mqtt")
	@Topic(value="test/mqtt1",messageType=MessageType.SENDER)
	static class Mc extends MqttCallback{
		
		override message(String topic, byte[] message) {
			System.err.println(new String(message,"UTF-8"));
			return null;
		}
		
	}
	def static void main(String[] args){
		MqttV3Service.subscriber(Executors.newCachedThreadPool,"c100.taomu.cool",1883,null,null,Mc);
	}
}