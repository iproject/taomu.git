/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.mqtt.broker

import cool.taomu.mqtt.broker.entity.MqttBrokerEntity
import cool.taomu.mqtt.broker.entity.SslEntity
import cool.taomu.util.YamlUtils

import static extension cool.taomu.util.YamlUtils.*

class MqttBrokerConfigure {

	new(){
	}

	def read() {
		return  YamlUtils.read("./config/broker.yml",MqttBrokerEntity)
	}

	def write(Object obj) {
		 YamlUtils.write("./config/broker.yml",obj);
	}

	def static void main(String[] args) {
		var a = new MqttBrokerConfigure();
		var b = new MqttBrokerEntity();
		b.anonymous = false;
		b.hostname = "127.0.0.1";
		b.ssl = new SslEntity();
		a.write = b;

	}
}
