/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.mqtt.broker.factory

import cool.taomu.mqtt.broker.utils.MessageTable
import cool.taomu.mqtt.broker.utils.MqttUtils
import io.netty.channel.ChannelHandlerContext
import io.netty.handler.codec.mqtt.MqttMessage
import io.netty.handler.codec.mqtt.MqttPubAckMessage
import org.slf4j.LoggerFactory

class PubAckRequest implements IProcess {
	val static LOG = LoggerFactory.getLogger(PubAckRequest);

	override request(ChannelHandlerContext ctx, MqttMessage mqttMessage) {
		var clientId = MqttUtils.getClientId(ctx.channel);
		if (clientId === null) {
			clientId = MqttUtils.getClientId(ctx.channel);
		}
		var puback = mqttMessage as MqttPubAckMessage;
		var msgId = puback.variableHeader.messageId
		MessageTable.table.remove(clientId,msgId);
		LOG.info("执行了MQTT PubAck 命令 clientId : {} messageId : {}", clientId, msgId);
	}
}
