/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.mqtt.broker.utils.impl

import com.google.common.cache.Cache
import com.google.common.cache.CacheBuilder
import cool.taomu.storage.inter.IStorage
import java.io.Serializable

class GuavaCache implements IStorage{
	val static Cache<String, Serializable> cache = CacheBuilder.newBuilder().build();
	val static instance = new GuavaCache();
	def static getInstance(){
		return instance;
	}
	
	override put(String identifier, String key, Serializable value) {
		cache.put(identifier+key,value);
		return 1;
	}
	
	override get(String identifier, String key) {
		cache.getIfPresent(identifier+key);
	}
	
	override remove(String identifier, String key) {
		cache.invalidate(identifier+key);
	}
	
	override clear(String identifier) {
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}
	
}
