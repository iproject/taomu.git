/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.mqtt.broker

import cool.taomu.mqtt.broker.factory.ProcessFactory
import io.netty.channel.ChannelHandlerContext
import io.netty.channel.ChannelInboundHandlerAdapter
import io.netty.handler.codec.mqtt.MqttMessage
import org.slf4j.LoggerFactory

class MQTTHandler extends ChannelInboundHandlerAdapter {
	val LOG = LoggerFactory.getLogger(MQTTHandler);

	override channelRead(ChannelHandlerContext ctx, Object obj) {
		val mqttMessage = obj as MqttMessage;
		if (mqttMessage !== null && mqttMessage.decoderResult.isSuccess) {
			var messageType = mqttMessage.fixedHeader.messageType;
			var process = ProcessFactory.instance(messageType);
			LOG.debug("收到请求类型：{}", messageType.name);
			process.request(ctx, mqttMessage);
		} else {
			ctx.close();
		}
	}
}
