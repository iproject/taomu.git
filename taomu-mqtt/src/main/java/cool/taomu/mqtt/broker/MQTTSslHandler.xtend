/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.mqtt.broker

import cool.taomu.mqtt.broker.entity.SslEntity
import cool.taomu.mqtt.broker.utils.SslUtil
import io.netty.channel.socket.SocketChannel
import io.netty.handler.ssl.ClientAuth
import io.netty.handler.ssl.SslContextBuilder
import io.netty.handler.ssl.SslHandler
import io.netty.handler.ssl.SslProvider

class MQTTSslHandler {
    def static build(SocketChannel channel,SslEntity config) {
        var sslConfig = config;
        var ssl = new SslUtil(sslConfig.keyStoreType);
        var kmf = ssl.keyManageFactory(sslConfig.keyFilePath, sslConfig.managerPwd, sslConfig.storePwd);
        var contextBuilder = SslContextBuilder.forServer(kmf);
        var sslContext = contextBuilder.sslProvider(SslProvider.valueOf("JDK")).build();
        var sslEngine = sslContext.newEngine(channel.alloc, channel.remoteAddress.hostString,
            channel.remoteAddress.port);
        sslEngine.useClientMode = false;
        if (sslConfig.useClientCA) {
            contextBuilder.clientAuth(ClientAuth.REQUIRE);
            contextBuilder.trustManager(ssl.trustManageFactory);
            sslEngine.needClientAuth = true;
        }
       return new SslHandler(sslEngine);
    }
}
