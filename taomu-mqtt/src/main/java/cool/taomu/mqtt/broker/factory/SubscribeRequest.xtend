/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.mqtt.broker.factory

import cool.taomu.mqtt.broker.entity.TopicEntity
import cool.taomu.mqtt.broker.impl.Publish
import cool.taomu.mqtt.broker.impl.PublishObservable
import cool.taomu.mqtt.broker.impl.RetainObservable
import cool.taomu.mqtt.broker.utils.MqttUtils
import cool.taomu.mqtt.broker.utils.inter.IObservable
import cool.taomu.mqtt.broker.utils.inter.IObserver
import io.netty.channel.ChannelHandlerContext
import io.netty.handler.codec.mqtt.MqttFixedHeader
import io.netty.handler.codec.mqtt.MqttMessage
import io.netty.handler.codec.mqtt.MqttMessageIdVariableHeader
import io.netty.handler.codec.mqtt.MqttMessageType
import io.netty.handler.codec.mqtt.MqttQoS
import io.netty.handler.codec.mqtt.MqttSubAckMessage
import io.netty.handler.codec.mqtt.MqttSubAckPayload
import io.netty.handler.codec.mqtt.MqttSubscribeMessage
import io.netty.handler.codec.mqtt.MqttTopicSubscription
import java.util.ArrayList
import java.util.List
import org.apache.oro.text.perl.Perl5Util
import org.slf4j.LoggerFactory

class SubscribeRequest implements IProcess {

	val static LOG = LoggerFactory.getLogger(SubscribeRequest);

	val static IObservable<IObserver> retainObservable = RetainObservable.instance;
	val static IObservable<IObserver> observable = PublishObservable.instance;

	override request(ChannelHandlerContext ctx, MqttMessage mqttMessage) {
		if(!(mqttMessage instanceof MqttSubscribeMessage)) return;
		var clientId = MqttUtils.getClientId(ctx.channel);
		if (clientId === null) {
			// 为perl mqtt客户端改进代码
			Thread.sleep(100)
			clientId = MqttUtils.getClientId(ctx.channel);
		}
		LOG.info("执行了MQTT Subscribe 命令 : " + clientId);
		try {
			var MqttSubscribeMessage subscribeMessage = mqttMessage as MqttSubscribeMessage;
			var int messageId = subscribeMessage.variableHeader().messageId();
			var validTopicList = registerTopics(ctx, subscribeMessage.payload().topicSubscriptions());
			synchronized (validTopicList) {
				if (validTopicList === null || validTopicList.size() == 0) {
					LOG.info(String.format("Valid all subscribe topic failure,messageId:%s", messageId));
					return;
				}
				var header = new MqttFixedHeader(MqttMessageType.SUBACK, false, MqttQoS.AT_MOST_ONCE, false, 0);
				var varHeader = MqttMessageIdVariableHeader.from(messageId);
				ctx.writeAndFlush(
					new MqttSubAckMessage(header, varHeader, new MqttSubAckPayload(getTopicQos(validTopicList))));
			}
		} catch (Exception ex) {
			LOG.debug("subscribe requst exception:", ex);
		}
	}

	def List<Integer> getTopicQos(List<TopicEntity> topics) {
		var qoss = new ArrayList<Integer>(topics.size());
		for (TopicEntity topic : topics) {
			qoss.add(topic.getQos());
		}
		return qoss;
	}

	def synchronized registerTopics(ChannelHandlerContext ctx, List<MqttTopicSubscription> topics) {
		synchronized (ctx) {
			var clientId = MqttUtils.getClientId(ctx.channel);
			var topicList = new ArrayList<TopicEntity>();
			for (MqttTopicSubscription subscription : topics) {
				val topic = new TopicEntity(subscription.topicName(), subscription.qualityOfService().value());
				topic.clientId = clientId;
				LOG.info("订阅Topic {} 的用户{}", topic.name, clientId);
				var p5 = new Perl5Util();
				if (p5.match("/^[A-Za-z0-9]+([\\/A-Za-z0-9_]*|\\/\\+||\\/\\#)$/", topic.name)) {
					var instance = new Publish(topic) as IObserver;
					observable.register(#[clientId, topic.name].join("#"), instance);
					retainObservable.publish(topic);
				}
				topicList.add(topic);
			}
			return topicList;
		}
	}

}
