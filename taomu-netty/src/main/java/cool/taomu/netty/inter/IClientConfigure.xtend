package cool.taomu.netty.inter

import io.netty.bootstrap.Bootstrap
import io.netty.channel.Channel
import io.netty.channel.ChannelPipeline
import io.netty.channel.socket.SocketChannel

interface IClientConfigure {
	def void bootstrap(Bootstrap bootstrap);
	def void pipeline(ChannelPipeline pipeline,SocketChannel ch);	
	def void channel(Channel channel);
}
