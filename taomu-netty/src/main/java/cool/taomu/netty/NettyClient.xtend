package cool.taomu.netty

import cool.taomu.netty.inter.IClientConfigure
import io.netty.bootstrap.Bootstrap
import io.netty.channel.ChannelFuture
import io.netty.channel.ChannelInitializer
import io.netty.channel.EventLoopGroup
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.SocketChannel
import io.netty.channel.socket.nio.NioSocketChannel
import javax.inject.Inject

class NettyClient {
	String host;
	int port;

	new(String host, int port) {
		this.host = host;
		this.port = port;
	}

	@Inject
	def void client(IClientConfigure config) {
		val coreNumber = Runtime.getRuntime().availableProcessors;
		var EventLoopGroup group = new NioEventLoopGroup(coreNumber);
		var Bootstrap bootstrap = new Bootstrap().group(group);
		config.bootstrap(bootstrap);
		bootstrap.channel(NioSocketChannel).handler(new ChannelInitializer<SocketChannel>() {
			override protected initChannel(SocketChannel ch) throws Exception {
				config.pipeline(ch.pipeline, ch);
			}
		});
		var ChannelFuture cf = bootstrap.connect(host, port).sync();
		config.channel(cf.channel);
		cf.channel().closeFuture().sync();
		group.shutdownGracefully();
	}
}
