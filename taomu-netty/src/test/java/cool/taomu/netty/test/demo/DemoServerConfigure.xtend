package cool.taomu.netty.test.demo

import cool.taomu.netty.inter.IServerConfigure
import io.netty.bootstrap.ServerBootstrap
import io.netty.buffer.ByteBuf
import io.netty.buffer.Unpooled
import io.netty.channel.ChannelHandlerContext
import io.netty.channel.ChannelInboundHandlerAdapter
import io.netty.channel.ChannelOption
import io.netty.channel.ChannelPipeline
import io.netty.channel.socket.SocketChannel

class DemoServerConfigure implements IServerConfigure {

	override bootstrap(ServerBootstrap bootstrap) {
		bootstrap.option(ChannelOption.SO_BACKLOG, 1024) // 设置TCP缓冲区
		.option(ChannelOption.SO_SNDBUF, 32 * 1024) // 设置发送缓冲大小
		.option(ChannelOption.SO_RCVBUF, 32 * 1024) // 这是接收缓冲大小
		.option(ChannelOption.SO_KEEPALIVE, true) // 保持连接
	}

	override pipeline(ChannelPipeline pipeline,SocketChannel ch) {
		pipeline.addLast(
			new ChannelInboundHandlerAdapter() {
				override void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
					var ByteBuf buf = msg as ByteBuf;
					var byte[] req = newByteArrayOfSize(buf.readableBytes())
					buf.readBytes(req);
					var String body = new String(req, "utf-8");
					System.out.println("Server :" + body);
					var String response = "返回给客户端的响应：" + body;
					ctx.writeAndFlush(Unpooled.copiedBuffer(response.getBytes()));
				}
			}
		)
	}

}
