package cool.taomu.netty.test.demo

import cool.taomu.netty.inter.IClientConfigure
import io.netty.bootstrap.Bootstrap
import io.netty.buffer.ByteBuf
import io.netty.buffer.Unpooled
import io.netty.channel.Channel
import io.netty.channel.ChannelHandlerContext
import io.netty.channel.ChannelInboundHandlerAdapter
import io.netty.channel.ChannelPipeline
import io.netty.channel.socket.SocketChannel
import io.netty.util.ReferenceCountUtil

class DemoClientConfigure implements IClientConfigure {

	override bootstrap(Bootstrap bootstrap) {
	}

	override pipeline(ChannelPipeline pipeline,SocketChannel ch) {
		pipeline.addLast(
			new ChannelInboundHandlerAdapter() {
				override void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
					try {
						var ByteBuf buf = msg as ByteBuf;
						var byte[] req = newByteArrayOfSize(buf.readableBytes())
						buf.readBytes(req);
						var String body = new String(req, "utf-8");
						System.out.println("Client :" + body);
					} finally {
						// 记得释放xxxHandler里面的方法的msg参数: 写(write)数据, msg引用将被自动释放不用手动处理; 但只读数据时,!必须手动释放引用数
						ReferenceCountUtil.release(msg);
					}
				}
			}
		)
	}

	override channel(Channel channel) {
		channel.writeAndFlush(Unpooled.copiedBuffer("Hello World!!!".getBytes()));
	}

}
