#! /bin/bash

RUN_NAME="taomu"
MAIN_CLASS="cool.taomu.Taomu"
JAVA_OPTS=""
CP="."
DIR=./deps/*.jar
CONFIG=./config/*
for i in $DIR ;do
    CP=$CP:$i
done
for i in $CONFIG ;do
    CP=$CP:$i
done


export JAVA_HOME=$JAVA_HOME

function start(){
    echo $JAVA_HOME
    echo $CP
    echo $MAIN_CLASS
    echo "$RUN_NAME trying to start ....."
    java -cp $CP $MAIN_CLASS $1 $2
    #nohup java -cp $CP $MAIN_CLASS $1 $2
    echo "$RUN_NAME started success."
}

function stop(){
    echo "Stopping $RUN_NAME ..."
    kill -9 `ps -ef|grep $RUN_NAME|grep -v grep|grep -v stop|awk '{print $2}'`
}

case "$1" in
    start)
        start
        ;;
    stop)
        stop
        ;;
    restart)
        stop 
        start
        ;;
    *)
        echo $"Usage: $0 {start|stop|restart}"
        exit 1
esac
    
