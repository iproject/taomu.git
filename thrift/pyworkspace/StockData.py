import sys
sys.path.append("../gen-py")

from cool.taomu.stock import StockApiPy 
from cool.taomu.stock.ttypes import *

from thrift.transport import *
from thrift.protocol import TCompactProtocol
from thrift.server import *

import baostock as bs

import socket

class StockDataPy:
    def __init__(self):
        pass

    def isDelisting(self,code = None):
        print(code)
        rs = bs.query_stock_basic(code=code)
        if(rs.error_code == '0')  & rs.next():
            data = rs.get_row_data()
            print(data)
            if data[5] == '1' and data[4] == '1':
                return True
        return False

    def getStockCodeData(self,code=None):
        bs.login()
        rs = bs.query_stock_industry(code)
        industry_list = []
        while (rs.error_code == '0') & rs.next():
            data = rs.get_row_data()
            if self.isDelisting(data[1]):
                industry_list.append(data)
        bs.logout()
        print(len(industry_list))
        return industry_list


def startServer():
    handler = StockDataPy()
    processor = StockApiPy.Processor(handler)
    transport = TSocket.TServerSocket("127.0.0.1",8091)
    tfactory = TCompactProtocol.TCompactProtocolFactory()
    server = TNonblockingServer.TNonblockingServer(processor,transport,tfactory,tfactory);

    server.serve()

if __name__ == '__main__':
   # handler = StockDataPy()
   # handler.getStockCodeData("sh.600000")
   # handler.getStockCodeData("sz.000001")
    startServer()

