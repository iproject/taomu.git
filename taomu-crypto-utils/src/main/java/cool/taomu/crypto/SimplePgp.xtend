/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.crypto

import cool.taomu.crypto.util.PgpUtils
import java.security.Security
import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.eclipse.xtend.lib.annotations.Accessors

@Accessors
class SimplePgp extends AbsCrypto {
	String key;
	String pass;

	new(byte[] src, String publicKey) {
		Security.addProvider(new BouncyCastleProvider());
		this.src = src;
		this.key = publicKey;
	}

	new(byte[] src, String privateKey, String passwd) {
		Security.addProvider(new BouncyCastleProvider());
		this.src = src;
		this.key = privateKey;
		this.pass = passwd;

	}

	new(ICrypto c, String publicKey) {
		Security.addProvider(new BouncyCastleProvider());
		this.c = c;
		this.key = publicKey;
	}

	new(ICrypto c, String privateKey, String passwd) {
		Security.addProvider(new BouncyCastleProvider());
		this.c = c;
		this.key = privateKey;
		this.pass = passwd;
	}

	override decode() {
		if (c !== null) {
			var bytes = PgpUtils.decrypt(this.c.data, this.key, this.pass);
			this.c.data = bytes;
			return c.decode();
		} else {
			return PgpUtils.decrypt(this.src, this.key, this.pass);
		}
	}

	override encode() {
		if (c !== null) {
			return PgpUtils.encrypt(this.c.encode(), this.key, true);
		} else {
			return PgpUtils.encrypt(this.src, this.key, true);
		}
	}
}
