package cool.taomu.jedis.queue;

import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;

import org.redisson.Redisson;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.auto.service.AutoService;

import cool.taomu.core.inter.AbsLock;
import cool.taomu.core.inter.ILock;

@AutoService(ILock.class)
public class RedisLock extends AbsLock {
	final static Logger LOG = LoggerFactory.getLogger(RedisLock.class);
	final static RedissonClient rClient;
	RLock lock;

	static {
		Config rconfig = new Config();
		rconfig.useSingleServer().setAddress("redis://192.168.1.223:6379");
		rClient = Redisson.create(rconfig);
	}

	@Override
	public void lock() {
		String uuid = UUID.randomUUID().toString();
		lock = rClient.getLock(uuid);
		lock.lock();
		LOG.info("添加锁");
	}

	@Override
	public void lockInterruptibly() throws InterruptedException {
		throw new UnsupportedOperationException("TODO: auto-generated method stub");
	}

	@Override
	public Condition newCondition() {
		throw new UnsupportedOperationException("TODO: auto-generated method stub");
	}

	@Override
	public boolean tryLock() {
		boolean ret = false;
		try {
			ret = this.tryLock(60, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return ret;
	}

	@Override
	public boolean tryLock(long arg0, TimeUnit arg1) throws InterruptedException {
		String uuid = UUID.randomUUID().toString();
		lock = rClient.getLock(uuid);
		lock.lock(arg0, arg1);
		LOG.info("添加锁");
		return lock.isLocked();
	}

	@Override
	public void unlock() {
		if (lock.isLocked()) {
			lock.unlock();
			LOG.info("释放锁");
		}
	}
}