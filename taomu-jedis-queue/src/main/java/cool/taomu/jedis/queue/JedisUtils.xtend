/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.jedis.queue

import java.util.HashSet
import org.apache.commons.pool2.impl.GenericObjectPoolConfig
import org.slf4j.LoggerFactory
import redis.clients.jedis.Connection
import redis.clients.jedis.HostAndPort
import redis.clients.jedis.Jedis
import redis.clients.jedis.JedisCluster
import redis.clients.jedis.JedisPool

/**
 * 名词 加ed 变 形容词
 * 动词 加ed 变 过去式
 * 动词 加ing 变 正在进行时
 */
class JedisUtils{
	val static LOG = LoggerFactory.getLogger(JedisUtils)
	var static JedisCluster cluster = null;
	var static JedisPool pool = null;
/* 
	def synchronized static getJedisPool(QueueConfigEntity config) {
		if (config !== null && pool === null) {
			var gopc = new GenericObjectPoolConfig<Jedis>();
			gopc.maxIdle = config.maxIdle;
			gopc.minIdle = config.minIdle;
			gopc.maxTotal = config.maxTotal;
			gopc.testOnBorrow = config.testOnBorrow;
			gopc.testOnReturn = config.testOnReturn;
			var password = config.password;
			var user = config.username;
			val hostAndPort = new HashSet<HostAndPort>();
			var timeout = config.timeout;
			config.hosts.forEach [ it |
				LOG.info("创建Redis连接: {}:{}", it.host, it.port);
				hostAndPort.add(new HostAndPort(it.host, it.port));
			]
			if (user === null) {
				if (password === null) {
					pool = new JedisPool(gopc, hostAndPort.get(0).host, hostAndPort.get(0).port);
				} else {
					pool = new JedisPool(gopc, hostAndPort.get(0).host, hostAndPort.get(0).port, timeout, password);
				}
			} else if (user !== null && password !== null) {
				pool = new JedisPool(gopc, hostAndPort.get(0).host, hostAndPort.get(0).port, timeout, user, password);
			}
		}
		return pool;
	}

	def synchronized static getJedisCluster(QueueConfigEntity config) {
		if (config !== null && cluster === null) {
			try {
				var gopc = new GenericObjectPoolConfig<Connection>();
				gopc.maxIdle = config.maxIdle;
				gopc.minIdle = config.minIdle;
				gopc.maxTotal = config.maxTotal;
				gopc.testOnBorrow = config.testOnBorrow;
				gopc.testOnReturn = config.testOnReturn;
				val hostAndPort = new HashSet<HostAndPort>();
				config.hosts.forEach [ it |
					LOG.info("创建Redis连接: {}:{}", it.host, it.port);
					hostAndPort.add(new HostAndPort(it.host, it.port));
				]
				var timeout = config.timeout;
				var maxAttempts = config.maxAttempts;
				var password = config.password;
				var user = config.username;
				if (user === null) {
					if (password === null) {
						cluster = new JedisCluster(hostAndPort, timeout, timeout, maxAttempts, gopc);
					} else {
						cluster = new JedisCluster(hostAndPort, timeout, timeout, maxAttempts, password, gopc);
					}
				} else if (user !== null && password !== null) {
					cluster = new JedisCluster(hostAndPort, timeout, timeout, maxAttempts, user, password, null, gopc);
				}
				Runtime.runtime.addShutdownHook(new Thread() {
					override run() {
						if (cluster !== null) {
							LOG.info("addShutdownHook 关闭了 Redis");
							cluster.close
						}
					}
				})
				return cluster;
			} catch (Exception e) {
				if (cluster !== null) {
					LOG.info("关闭了 Redis");
					cluster.close
				}
				if (pool !== null) {
					LOG.info("关闭了 Redis");
					pool.close
				}
				LOG.error("创建Redis失败", e);
				cluster = null;
				pool = null;
			}
		}
	}
*/
}
