package cool.taomu.jedis.queue

import java.util.List
import org.apache.commons.lang3.StringUtils
import org.eclipse.xtend.lib.annotations.Accessors

@Accessors
class JedisConfigEntity {
	@Accessors
	static class Host{
		String host;
		int port;
	}
	String shareTaskQueue;
	String taskQueue
	String driver;	
	int maxIdle;
	int minIdle;
	int maxTotal;
	boolean testOnBorrow;
	boolean testOnReturn;
	String username;
	String password;
	int timeout;
	int maxAttempts;
	List<Host> hosts;
	
	def getShareTaskQueue(){
		return StringUtils.defaultIfBlank(this.shareTaskQueue,"globalQueue")
	}
	
	def getTaskQueue(){
		return StringUtils.defaultIfBlank(this.taskQueue,this.getShareTaskQueue())
	}
}