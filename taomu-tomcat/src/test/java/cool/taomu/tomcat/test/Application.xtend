package cool.taomu.tomcat.test

import cool.taomu.guice.TaomuGuice
import cool.taomu.guice.ann.Binder
import cool.taomu.guice.ann.Binders
import cool.taomu.tomcat.embed.TomcatServer
import cool.taomu.tomcat.test.controller.DemoController
import cool.taomu.tomcat.test.entity.UserEntity

@Binders(name="/user", value=#[
	@Binder(named="/user/demo/user", to=DemoController)
])
class Application {

	def static void main(String[] args) {
		var a = TaomuGuice.entry("/user", Application);
		new TomcatServer().start(a);
	}
}
