package cool.taomu.tomcat.test.entity

import cool.taomu.servlet.entity.AbsEventEntity
import org.eclipse.xtend.lib.annotations.Accessors
import org.eclipse.xtend.lib.annotations.ToString

@Accessors
@ToString
class UserEntity extends AbsEventEntity{
	String username;
}