package cool.taomu.tomcat.test.controller

import com.google.common.eventbus.Subscribe
import cool.taomu.servlet.Topic
import cool.taomu.tomcat.test.entity.UserEntity

class DemoController {
	
	@Subscribe
	@Topic("user")
	def void a(UserEntity user){
		println(user);
		user.Return("{value:Hello World}")
	}	

	@Subscribe
	def void b(UserEntity user){
		//user.Return("{value:Hello World2}")
	}
}
