package cool.taomu.servlet

import java.lang.annotation.ElementType
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.lang.annotation.Target

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
annotation Topic {
	String value	
}

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
annotation Async{ }


