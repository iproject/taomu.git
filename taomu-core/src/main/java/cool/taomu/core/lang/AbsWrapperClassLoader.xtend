/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.core.lang

abstract class AbsWrapperClassLoader extends ClassLoader implements AutoCloseable {
	protected ClassLoader classLoader;
	protected AutoCloseable autoClose;

	abstract def void load(String arg,ClassLoader loader) throws Exception;

	def wrapper(Object wrap) {
		this.classLoader = wrap as ClassLoader;
		this.autoClose = wrap as AutoCloseable;
	}

	override close() throws Exception {
		if (this.autoClose !== null) {
			this.autoClose.close
		}
	}

	override findClass(String name) {
		if (this.classLoader !== null) {
			return this.classLoader.loadClass(name);
		}
	}
}
