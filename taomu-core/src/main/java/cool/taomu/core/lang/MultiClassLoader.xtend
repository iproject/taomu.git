/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.core.lang

import cool.taomu.core.entity.TaskEntity.ScriptType
import java.io.IOException
import java.util.concurrent.ConcurrentSkipListMap
import org.apache.groovy.util.concurrent.concurrentlinkedhashmap.ConcurrentLinkedHashMap
import org.apache.groovy.util.concurrent.concurrentlinkedhashmap.Weighers
import org.eclipse.xtend.lib.annotations.Accessors

/**
 * @startuml
 * class MultiClassLoader [[java:cool.taomu.core.lang.MultiClassLoader]] {
 * #{static} mcache: ConcurrentSkipListMap<TaskEntity.ScriptType,ConcurrentLinkedHashMap<String,AbsWrapperClassLoader>>
 * -{static} instance: MultiClassLoader
 * -MultiClassLoader()
 * +{static} getInstnace(): MultiClassLoader
 * +add(stype: TaskEntity.ScriptType, flag: String, script: String, awclassloader: AbsWrapperClassLoader): void
 * +getWrapperClassLoader(stype: TaskEntity.ScriptType, flag: String): AbsWrapperClassLoader
 * +findClass(name: String): Class<?>
 * }
 * class ClassLoader {
 * }
 * ClassLoader <|-- MultiClassLoader
 * @enduml
 */
@Accessors
class MultiClassLoader extends ClassLoader {
	// type|flag|classloader
	protected final static ConcurrentSkipListMap<ScriptType, ConcurrentLinkedHashMap<String, AbsWrapperClassLoader>> mcache = new ConcurrentSkipListMap();
	static val instance = new MultiClassLoader();

	private new() {
	}

	def static getInstnace() {
		return instance;
	}

	def void add(ScriptType stype, String flag, String script, AbsWrapperClassLoader awclassloader) {
		var cache = mcache.getOrDefault(stype,
			new ConcurrentLinkedHashMap.Builder<String, AbsWrapperClassLoader>().maximumWeightedCapacity(512).weigher(
				Weighers.singleton()).listener(
				[ key, value |
					try {
						value.close();
					} catch (IOException e) {
						throw new RuntimeException(e);
					}
				]
			).build());
		if (!script.nullOrEmpty) {
			awclassloader.load(script, Thread.currentThread.contextClassLoader);
			cache.put(flag, awclassloader);
		}
		if (cache.size == 1) {
			mcache.put(stype, cache);
		}
	}

	def getWrapperClassLoader(ScriptType stype, String flag) {
		return mcache.get(stype).get(flag);
	}

	override findClass(String name) {
		if (mcache !== null && mcache.size > 0) {
			var values = mcache.values;
			for (i : 0 .. values.size) {
				var cache = values.get(i);
				var loader = cache.values.findFirst [
					return it.loadClass(name) !== null;
				]
				return loader.loadClass(name);
			}
		}
		return null;
	}
}
