/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.core.lang

import cool.taomu.core.entity.TaskEntity.ScriptType
import java.lang.annotation.Documented
import java.lang.annotation.Retention
import java.lang.annotation.Target

@Documented
@Retention(RUNTIME)
@Target(#[TYPE])
annotation WrapperClassLoader {
	ScriptType value = ScriptType.None;
}
