/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.core.entity

import java.io.Serializable
import java.util.concurrent.TimeUnit
import org.eclipse.xtend.lib.annotations.Accessors

@Accessors
final class TimerEntity  implements Serializable {
	enum Type {
		Schedule,
		Schedule_at_fixed_rate,
		Schedule_with_fixed_delay
	}
	enum Operation{
		Start,Stop
	}
	Operation operation = Operation.Start;
	Type type = Type.Schedule_with_fixed_delay;
	Long initialDelay = 60L;
	Long delay = 60L;
	TimeUnit timeUit = TimeUnit.SECONDS;
}
