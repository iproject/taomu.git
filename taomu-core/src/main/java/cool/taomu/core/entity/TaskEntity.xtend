/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 * http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.core.entity

import java.io.Serializable
import java.util.UUID
import org.eclipse.xtend.lib.annotations.Accessors
import org.eclipse.xtend.lib.annotations.ToString

@Accessors
@ToString
final class TaskEntity implements Serializable {
	enum ScriptType {
		None,
		Groovy,
		JavaScript,
		Jython,
		JRuby,
		Bsh
	}

	enum DeplyType {
		None,
		Deply,
		Redeployment
	}

	String uuid;
	String main_class;
	String queue = "globalQueue";
	DeplyType deplyType = DeplyType.None;
	ScriptType scriptType = ScriptType.None;
	String script;
	String payload;
	TimerEntity timer;
	new(String queue){
		this()
		this.queue = queue;	
	}
	new() {
		uuid = UUID.randomUUID.toString;
	}
	def setQueue(String queue){
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}
}
