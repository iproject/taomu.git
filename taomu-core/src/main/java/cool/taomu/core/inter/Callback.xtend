package cool.taomu.core.inter

import java.io.Serializable

interface Callback<T> extends Serializable{
	def  void execute(T instance);
}