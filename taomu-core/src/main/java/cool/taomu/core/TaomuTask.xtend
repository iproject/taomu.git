/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.core

import cool.taomu.core.entity.TaomuConfigEntity
import cool.taomu.core.entity.TaskEntity
import cool.taomu.core.inter.ITaskQueue
import cool.taomu.util.TaomuServiceLoader
import cool.taomu.util.TimerLoop
import cool.taomu.util.YamlUtils
import java.util.ArrayList
import java.util.Iterator
import java.util.List
import java.util.concurrent.ArrayBlockingQueue
import javax.swing.Timer
import org.slf4j.LoggerFactory

class TaomuTask {
	val static LOG = LoggerFactory.getLogger(TaomuTask);
	val static instance = new TaomuTask();
	val ArrayBlockingQueue<Runnable> workQueue;
	val List<ITaskQueue> inq = new ArrayList();
	val List<TimerLoop> timerLoops = new ArrayList();
	TaomuConfigEntity config;

	private new() {
		config = YamlUtils.read("./config/taomu.yml", TaomuConfigEntity);
		LOG.info("设置的线程池队列大小为:{}", Integer.valueOf(config.getCapacity()));
		workQueue = new ArrayBlockingQueue<Runnable>(config.capacity);
		var Iterator<Class<ITaskQueue>> inqs = new TaomuServiceLoader<ITaskQueue>().loader(ITaskQueue).iterator();
		inqs.forEach [
			var ITaskQueue ine;
			try {
				ine = it.newInstance() as ITaskQueue;
				ine.init();
				var tloops = ine.trigger([
					LOG.info("触发任务执行");
					TaomuRuntime.instance.execute(it);
				]);
				if(tloops !== null){
					timerLoops.add(tloops);
				}
				inq.add(ine);
			} catch (InstantiationException | IllegalAccessException e) {
				e.printStackTrace();
			}
		]
		TaomuThreadPoolFactory.init(workQueue, inq);
		timerLoops.forEach[it.start]
	}

	def static getInstance() {
		return instance;
	}

	def add(TaskEntity task) {
		synchronized (this) {
			LOG.info("{}:任务添加到队列", task.main_class);
			inq.filterNull.forEach [
				it.add(new TaomuRunnable(task));
			]
		}
	}

}
