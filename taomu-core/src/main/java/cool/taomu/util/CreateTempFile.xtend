/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.util

import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter
import java.nio.charset.Charset
import java.util.UUID
import org.apache.commons.io.FileUtils

class CreateTempFile {
    def static create(byte[] content,String charset) {
        return create(new String(content,Charset.forName(charset)));
    }
    def static create(String content) {
        var tempName = UUID.randomUUID.toString();
        var temp = File.createTempFile(tempName, ".temp");
        temp.deleteOnExit();
        try(var bw = new BufferedWriter(new FileWriter(temp))) {
            bw.write(content);
        }
        return temp;
    }
    
    def static create(byte[] content) {
        var tempName = UUID.randomUUID.toString();
        var temp = File.createTempFile(tempName, ".temp");
        temp.deleteOnExit();
        FileUtils.writeByteArrayToFile(temp,content);
        return temp;
    }
}
