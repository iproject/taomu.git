/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.util

import java.io.FileReader
import org.yaml.snakeyaml.DumperOptions
import org.yaml.snakeyaml.Yaml

class YamlUtils {

	def static <T> read(String path,Class<T> entity) {
		return new Yaml().loadAs(new FileReader(path), entity);
	}

	def static write(String path,Object obj) {
		var DumperOptions options = new DumperOptions();
		options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK); // 通常使用的yaml格式
		options.setDefaultScalarStyle(DumperOptions.ScalarStyle.PLAIN);
		var yml = new Yaml(options);
		var String str = yml.dump(obj);
		System.out.println(str);
	}
}
