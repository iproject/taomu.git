/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.util

import java.lang.annotation.Annotation
import java.net.URL
import java.util.Enumeration
import java.util.Objects
import java.util.concurrent.ConcurrentSkipListMap
import org.apache.commons.io.IOUtils
import org.apache.commons.lang3.ObjectUtils
import org.slf4j.LoggerFactory

class TaomuServiceLoader<E> {
	val static LOG = LoggerFactory.getLogger(TaomuServiceLoader);
	val PREFIX = "META-INF/services/";
	val ConcurrentSkipListMap<String, Class<E>> providers = new ConcurrentSkipListMap();

	new() {
	}

	def loader(Class<E> svc) {
		return this.loader(svc, Thread.currentThread.contextClassLoader);
	}

	def loader(Class<E> svc, ClassLoader cloader) {
		val service = Objects.requireNonNull(svc, "class 不能为 null");
		val loader = ObjectUtils.defaultIfNull(cloader, ClassLoader.getSystemClassLoader());
		val fullName = PREFIX + service.name;
		LOG.info("fullName : {}", fullName);
		var Enumeration<URL> configs;
		if (loader === null) {
			configs = ClassLoader.getSystemResources(fullName);
		} else {
			configs = loader.getResources(fullName);
		}
		LOG.info("测试此枚举是否包含更多元素:{}", configs.hasMoreElements);
		while (configs.hasMoreElements) {
			var strs = IOUtils.toString(configs.nextElement.openStream, "UTF-8");
			strs.split("\n").filterNull.forEach [
				try {
					LOG.info("加载:{}",it)
					var zlass = Class.forName(it, false, loader);
					providers.put(zlass.simpleName, zlass as Class<E>);
				} catch (ClassNotFoundException ex) {
					LOG.info("类 " + it + " 不存在", ex)
				}
			];
		}
		return this;
	}

	def iterator() {
		return providers.values.iterator;
	}

	def get(String name) {
		return providers.get(name);
	}
	
	def nullOrEmpty(){
		return providers === null || providers.size == 0
	}

	def get(Annotation ann) {
	}
}
