package cool.taomu.indexed.lmdb

import com.google.auto.service.AutoService
import cool.taomu.indexed.inter.IIndexed
import cool.taomu.indexed.lmdb.util.LmdbUtils
import java.util.concurrent.ConcurrentHashMap
import cool.taomu.indexed.impl.Indexed

@AutoService(IIndexed)
class IndexedImdb  extends Indexed{
	LmdbUtils lmdb = new LmdbUtils();
	ConcurrentHashMap<String,String> map = new ConcurrentHashMap();
	String name;
	
	override open(String name) {
		lmdb.dbName = name;
		this.name = lmdb.dbName;
	}
	
	override open(String name, String version) {
		lmdb.dbName = #[name,version].join("@");
		this.name = lmdb.dbName;
	}
	
	override createKeyPath(String id) {
		map.put(this.name,id)
	}
	
	override createIndex(String fieldName, boolean unique) {
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}
	
	override get(String key) {
		return lmdb.get(key);
	}
	
	override put(String result) {
		var Object keyPath = this.getKeyPath(map.get(this.name),result);
		lmdb.put(keyPath as String,result);
	}
	
	
	def static void main(String[] args){
		var im = new IndexedImdb();
		im.open("aa");
		im.createKeyPath("id");
		im.put("{\"id\":\"aa\",\"name\":\"bb\"}");
		println(im.get("aa"))
	}
	
}