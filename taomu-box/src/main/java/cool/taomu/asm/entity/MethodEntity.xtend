/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.asm.entity

import org.eclipse.xtend.lib.annotations.Accessors
import org.eclipse.xtend.lib.annotations.ToString
import org.objectweb.asm.Opcodes

@Accessors
@ToString
class MethodEntity {
	int access = Opcodes.ACC_PUBLIC;
	String internalName;
	String defineMethod;
	boolean defaultPackage = false;

	new(String defineMethod) {
		this(defineMethod, false)
	}

	new(String defineMethod, boolean defaultPackage) {
		this.defaultPackage = defaultPackage;
		this.defineMethod = defineMethod;
	}

	new(String internalName, String defineMethod) {
		this.defineMethod = defineMethod;
		this.internalName = internalName;
	}

	new(String internalName, String defineMethod, boolean defaultPackage) {
		this.defaultPackage = defaultPackage;
		this.defineMethod = defineMethod;
		this.internalName = internalName;
	}
}
