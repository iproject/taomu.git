/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.util

import cool.taomu.guice.TaomuGuice
import cool.taomu.guice.ann.Binder
import cool.taomu.guice.ann.Binders
import cool.taomu.guice.ann.Box
import java.util.Collection
import java.util.ServiceLoader
import java.util.concurrent.ConcurrentSkipListMap
import org.slf4j.LoggerFactory

interface ITaomuServiceLoader<E> {
	def E get(String name);

	def E get(Class<E> zlass);

	def E first();

	def Collection<E> getAll();
}

class TaomuServiceLoader<E> implements ITaomuServiceLoader<E> {
	val static LOG = LoggerFactory.getLogger(TaomuServiceLoader);
	val ConcurrentSkipListMap<String, E> providers = new ConcurrentSkipListMap();
	Class<E> zlass;
	ClassLoader loader;

	new(Class<E> zlass) {
		this(zlass, Thread.currentThread.contextClassLoader);
	}

	new(Class<E> zlass, ClassLoader loader) {
		this.zlass = zlass;
		this.loader = loader;
	}

	private synchronized def init() {
		if (providers.isEmpty) {
			var ServiceLoader<E> serviceLoader = ServiceLoader.load(zlass, loader);
			serviceLoader.iterator.forEach [ s |
				LOG.info("加载 {}", s.class.name);
				if (s.class.getAnnotation(Box) !== null || s.class.getAnnotation(Binder) !== null ||
					s.class.getAnnotation(Binders) !== null) {
					LOG.info("解析Box");
					providers.put(s.class.name, TaomuGuice.entry(s.class).getInstance(s.class) as E);
				} else {
					providers.put(s.class.name, s);
				}
			]
		}
	}

	override get(String name) {
		init();
		return providers.get(name);
	}

	override getAll() {
		init();
		return providers.values;
	}

	override first() {
		init();
		return providers.values.get(0);
	}

	override get(Class<E> zlass) {
		return this.get(zlass.name);
	}

}
