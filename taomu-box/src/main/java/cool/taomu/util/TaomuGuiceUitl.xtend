package cool.taomu.util

import cool.taomu.guice.ann.Binders
import cool.taomu.guice.entity.BindersEntity
import cool.taomu.guice.BinderRule

class TaomuGuiceUitl {
	def static toBinders(Binders bs) {
		var entity = new BindersEntity();
		if (bs.dependence.size > 0) {
			entity.dependence = bs.dependence;
		}
		if (!bs.name.nullOrEmpty) {
			entity.name = bs.name;
		}
		if (!bs.properties.nullOrEmpty) {
			entity.properties = bs.properties;
		}
		if (bs.installs.size > 0) {
			entity.installs = bs.installs;
		}
		if (bs.value.size > 0) {
			entity.values = BinderRule.toBinderEntity(bs.value);
		}
		return entity
	}
}
