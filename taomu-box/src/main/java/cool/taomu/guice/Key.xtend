package cool.taomu.guice

import java.lang.annotation.Annotation

class Key<T> extends com.google.inject.Key<T> {
	def static get(Annotation ann)	{
		return get(Object,ann);
	}
}