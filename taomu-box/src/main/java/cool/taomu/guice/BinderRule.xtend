package cool.taomu.guice

import com.google.inject.name.Names
import cool.taomu.asm.CreateMethod
import cool.taomu.asm.entity.MethodEntity
import cool.taomu.guice.ann.Binder
import cool.taomu.guice.entity.BinderEntity
import cool.taomu.guice.entity.Scope
import java.util.ArrayList
import java.util.List
import org.objectweb.asm.Type

class BinderRule {
	def static toBinderEntity(Binder[] binders) {
		val results = new ArrayList<BinderEntity>(binders.size);
		binders.filterNull.forEach [
			var entity = new BinderEntity;
			entity.provider = it.provider;
			entity.in = it.in;
			if (!it.named.nullOrEmpty) {
				entity.named = it.named;
			}
			if (it.bind !== null) {
				entity.bind = it.bind;
			}
			if (it.to !== null && it.to !== Void) {
				entity.to = it.to;
			}

			results.add(entity);
		]
		return results;
	}

	def static void rule(CreateMethod cm, List<BinderEntity> binders) {
		val bind = new MethodEntity("com/google/inject/Binder",
			"com/google/inject/binder/AnnotatedBindingBuilder bind(java/lang/Class)", true);
		val to = new MethodEntity("com/google/inject/binder/AnnotatedBindingBuilder",
			"com/google/inject/binder/ScopedBindingBuilder to(java/lang/Class)", true);
		val toProvider = new MethodEntity("com/google/inject/binder/AnnotatedBindingBuilder",
			"com/google/inject/binder/ScopedBindingBuilder toProvider(java/lang/Class)", true);
		val in = new MethodEntity("com/google/inject/binder/ScopedBindingBuilder", "void in(com/google/inject/Scope)",
			true);
		binders.filterNull.forEach [
			if (it.bind !== null) {
				cm.load("binder").ldc(Type.getType(it.bind)).invokeInterface(bind)
				if (it.named !== null) {
					cm.ldc(it.named);
					cm.invokeStatic(
						new MethodEntity("com/google/inject/name/Names",
							"com/google/inject/name/Named named(java/lang/String)", true))
					cm.invokeInterface(
						new MethodEntity("com/google/inject/binder/AnnotatedBindingBuilder",
							"com/google/inject/binder/LinkedBindingBuilder annotatedWith(java/lang/annotation/Annotation)",
							true));
				}
				if (!it.to.equals(Void)) {
					if (it.provider) {
						cm.ldc(Type.getType(it.to)).invokeInterface(toProvider);
					} else {
						cm.ldc(Type.getType(it.to)).invokeInterface(to);
					}
					if (it.in !== Scope.None) {
						if (it.in.equals(Scope.SINGLETON)) {
							cm.getStatic("com/google/inject/Scopes", "SINGLETON", "Lcom/google/inject/Scope;");
						} else if (it.in.equals(Scope.NO_SCOPE)) {
							cm.getStatic("com/google/inject/Scopes", "NO_SCOPE", "Lcom/google/inject/Scope;");
						}
						cm.invokeInterface(in);
					} else {
						cm.pop
					}
				}
			} 
		]
	}
}
