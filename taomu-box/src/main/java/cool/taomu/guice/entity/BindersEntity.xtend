package cool.taomu.guice.entity

import java.util.List
import org.eclipse.xtend.lib.annotations.Accessors
import com.google.inject.Module

@Accessors
class BindersEntity {
	String name;
	String properties;
	Class<? extends Module>[] installs;
	List<BinderEntity>	values;
	String[] dependence;
}