package cool.taomu.guice.entity

import org.eclipse.xtend.lib.annotations.Accessors

enum Scope {
	None,
	NO_SCOPE,
	SINGLETON
}

@Accessors
class BinderEntity {
	Class<?> bind = Object;
	String named
	Class<?> to;
	boolean provider = false;
	Scope in = Scope.None;
}
