package cool.taomu.guice.test

import cool.taomu.guice.ann.Binder
import cool.taomu.util.TaomuServiceLoader
import javax.inject.Inject
import org.junit.Test

interface IslTest {
	def void println();
}

class SlTestImpl implements IslTest {
	override println() {
		System.err.println("Hello Service Loader");
	}
}

class P {
	def void p() {
		println("p");
	}
}

class SlTestImpl3 implements IslTest {

	override println() {
		System.err.println("Hello Guice Service Loader 333");
	}
}

@Binder(bind=IslTest, to=SlTestImpl3)
class SlTestImpl2 implements IslTest {
	@Inject
	IslTest p;

	override println() {
		p.println();
		System.err.println("Hello Guice Service Loader");
	}
}

class TaomuServiceLoaderTest {

	@Test
	def void t() {
		var sloader = new TaomuServiceLoader(IslTest);
		//sloader.first.println
		//sloader.get(SlTestImpl.name).println
		sloader.get(SlTestImpl2.name).println
	}
}
