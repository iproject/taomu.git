package cool.taomu.guice.test

import cool.taomu.guice.demo.mybatisDemo.module.DefaultModule
import java.io.PrintWriter
import org.objectweb.asm.ClassReader
import org.objectweb.asm.ClassWriter
import org.objectweb.asm.util.TraceClassVisitor

class ClassToBytecode {
    def static toBytecode(Class<?> zlass) {
        var classReader = new ClassReader(zlass.name);
        var classWriter = new ClassWriter(ClassWriter.COMPUTE_MAXS + ClassWriter.COMPUTE_FRAMES);
        var classVisitor = new TraceClassVisitor(classWriter, new PrintWriter(System.err));
        classReader.accept(classVisitor, ClassReader.EXPAND_FRAMES);
    }
    
    def static void main(String[] args){
    	ClassToBytecode.toBytecode(DefaultModule);
    }
}
