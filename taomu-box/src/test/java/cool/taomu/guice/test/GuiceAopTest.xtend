package cool.taomu.guice.test

import com.google.inject.AbstractModule
import com.google.inject.Guice
import com.google.inject.matcher.Matchers
import cool.taomu.guice.TaomuGuice
import cool.taomu.guice.ann.Binder
import cool.taomu.guice.ann.Binders
import cool.taomu.guice.entity.InterceptorEntity
import java.lang.annotation.ElementType
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.lang.annotation.Target
import org.aopalliance.intercept.MethodInterceptor
import org.aopalliance.intercept.MethodInvocation
import org.junit.Test

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
annotation GET {
}

class AopInterceptor implements MethodInterceptor {

	override invoke(MethodInvocation invocation) throws Throwable {
		println("Hello ");
		invocation.proceed();
	}
}

@Binders(name="Aop", installs=AopModule)
class AopModule extends AbstractModule {
	override configure() {
		var binder = binder();
		// var ai = new AopInterceptor()
		// requestInjection(ai);
		binder.bindInterceptor(Matchers.any(), Matchers.annotatedWith(GET), new AopInterceptor());
	}
}

class AModule extends AbstractModule {
	override configure() {
		var binder = binder();
		//binder.install(new AopModule())
		binder.bind(IA).to(AImple)
	}
}

interface IA {
	def void a(String a);
}

class AImple implements IA {

	@GET
	override a(String a) {
		println("HA");
	}

}

@Binders(name="aa",value=#[
	@Binder(bind=IA, to=AImple)
])
class GuiceAopTest {

	@Test
	def a() {
		var in = Guice.createInjector(new AopModule()).createChildInjector(new AModule());
		var a = in.getInstance(AImple)
		a.a("");
	}

	@Test
	def void b() {
		
		var in = TaomuGuice.entry("aa",#[new InterceptorEntity(Matchers.any(),Matchers.annotatedWith(GET),new AopInterceptor())],GuiceAopTest)
		var a = in.getInstance(AImple)
		a.a("");
	}
}
