package cool.taomu.guice.test

import com.google.inject.AbstractModule
import com.google.inject.Guice
import com.google.inject.Provider
import com.google.inject.matcher.Matchers
import com.google.inject.name.Names
import cool.taomu.guice.TaomuGuice
import cool.taomu.guice.ann.Binder
import cool.taomu.guice.ann.Binders
import cool.taomu.guice.entity.BinderEntity
import cool.taomu.guice.entity.InterceptorEntity
import cool.taomu.util.PropertyUtils
import javax.inject.Inject
import javax.inject.Named
import org.eclipse.xtend.lib.annotations.Accessors
import org.junit.Test

import static org.junit.Assert.assertEquals
import cool.taomu.guice.entity.BindersEntity

interface ITest {
	def String p();
}

class TestImpl implements ITest {
	override p() {
		return "Hello World";
	}
}

class TestImpl2 implements ITest {
	override p() {
		return "Hello World2";
	}
}

@Binders(name="test_imple1", value=#[
	@Binder(bind=ITest, to=TestImpl, named="impl1")
])
@Binders(name="test_imple2", value=#[
	@Binder(bind=ITest, to=TestImpl2, named="impl1")
])
@Accessors
class TestNameImpl1 {
	ITest t;

	@Inject
	def void init(@Named("impl1") ITest t) {
		this.t = t;
	}
}

class TestProvider implements Provider<ITest> {

	override get() {
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}

}

class TestModule extends AbstractModule {
	override configure() {
		var binder = binder();
		binder.bind(ITest).annotatedWith(Names.named("impl1")).to(TestImpl)
		binder.bind(ITest).annotatedWith(Names.named("impl2")).toProvider(TestProvider);
	}
}

class TestModule3 extends AbstractModule {
	override configure() {
	}
}

class TestModule2 extends AbstractModule {
	override configure() {
		var binder = binder();
		Names.bindProperties(binder, PropertyUtils.load("", getClass()))
		// Names.bindProperties(binder,null);
		binder.install(new TestModule3())
		binder.bind(ITest).annotatedWith(Names.named("impl1")).to(TestImpl2)
	}
}

//@Binders(value="demo", rule=#[
//	@Binder(bind=ITest, to=TestImpl, in=Scope.SINGLETON, named="impl1"),
// @Binder(bind=ITest, to=TestImpl2, in=Scope.NO_SCOPE, named="impl2"),
/*@Binder(bind=ITest, named="impl3", instance=TestImpl3, args=#[
 * 	@Args(type=String, strval="127.0.0.1"),
 * 	@Args(type=int, intval=30001),
 * 	@Args(type=char, charval='c'),
 * 	@Args(type=Class, classval=TestImpl3)
 ])*/
//])
class GuiceTest {
	@Test
	def void testATestNameImpl1() {
		var res1 = Guice.createInjector(new TestModule).getInstance(TestNameImpl1).t.p;
		var res2 = TaomuGuice.entry("test_imple1", TestNameImpl1).getInstance(TestNameImpl1).t.p;
		println(#[res1, res2].join("==>"))
		assertEquals(res1, res2)
	}

	@Test
	def void testATestNameImpl2() {
		var r1 = Guice.createInjector(new TestModule).getInstance(TestNameImpl1).t.p;
		var r2 = Guice.createInjector(new TestModule2).getInstance(TestNameImpl1).t.p;
		println(#[r1, r2].join("==>"))
	}

	@Test
	def void testATestNameImpl3() {
		var r1 = TaomuGuice.entry("test_imple1", TestNameImpl1).getInstance(TestNameImpl1).t.p;
		var r2 = TaomuGuice.entry("test_imple2", TestNameImpl1).getInstance(TestNameImpl1).t.p;
		println(#[r1, r2].join("==>"))
	}

	@Test
	def void testATestNameImpl4() {
		var r1 = Guice.createInjector(new TestModule3).getInstance(TestNameImpl1).t.p;
		println(#[r1].join("==>"))
	}

	def static void main(String[] args) {
	// var i = Guice.createInjector(new TestModule);
	// i.getInstance(Test2).t.p;
	/*var itest = TaomuGuice.entry(GuiceTest).getInstance(Test2);
	 * itest.t2.p;
	 * itest.t.p;
	 * itest.t3.p;
	 * 
	 */
	}
}
