package cool.taomu.guice.demo.mybatisDemo.service

import cool.taomu.guice.demo.mybatisDemo.entity.User
import java.util.List

interface IUserService {
	def List<User> getUser();
    def User getUserByName(String userName);
    def User getUserByAge(int age);	
}

interface IAuthService {
	def List<User> getUser();
    def User getUserByName(String userName);
    def User getUserByAge(int age);	
}