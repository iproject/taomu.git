package cool.taomu.guice.demo;

import com.google.inject.Injector;

import cool.taomu.guice.TaomuGuice;
import cool.taomu.guice.ann.Binder;
import cool.taomu.guice.ann.Binders;
import cool.taomu.guice.demo.service.IStudent;
import cool.taomu.guice.demo.service.StudentImpl;

@Binders(name="demo",value={
	@Binder(bind=IStudent.class,to=StudentImpl.class)
})
public class Application {
	public static void main(String[] args) {
		Injector injector = TaomuGuice.entry("demo",Application.class);
		IStudent  stu = injector.getInstance(IStudent.class);
		stu.print();
	}
}
