package cool.taomu.guice.demo.mybatisDemo.module

import cool.taomu.guice.demo.mybatisDemo.entity.UserMapper
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory
import org.mybatis.guice.MyBatisModule
import org.mybatis.guice.datasource.builtin.PooledDataSourceProvider
import org.mybatis.guice.datasource.helper.JdbcHelper

class MybaitsModule extends MyBatisModule {
	
	override protected initialize() {
		var binder = binder();
		binder.install(JdbcHelper.H2_EMBEDDED);
		// DataSource
		bindDataSourceProviderType(PooledDataSourceProvider);
		bindTransactionFactoryType(JdbcTransactionFactory);
		mapUnderscoreToCamelCase(true);
		// 数据库操作
		addMapperClass(UserMapper);
		environmentId("dev")
	}
}
