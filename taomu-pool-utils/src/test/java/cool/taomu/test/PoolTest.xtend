package cool.taomu.test

import cool.taomu.pool.SoftReferencePoolUtils
import java.util.ArrayList

class PoolTest {
	def static void main(String[] args) {
		var sl = new SoftReferencePoolUtils(ArrayList.name);
		try(var i = sl.instance){
			System.out.println(i.id);
			var a = i.instance as ArrayList;
			System.out.println(a.add("aa"));
			System.out.println(a.toString);
			a.clear
		}
		try(var j = sl.instance){
			System.out.println(j.id);
			var a = j.instance as ArrayList;
			System.out.println(a.add("bb"));
			System.out.println(a.toString);
			a.clear
		}
	}
}