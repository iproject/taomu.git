package cool.taomu.pool

import java.util.concurrent.atomic.AtomicLong
import org.apache.commons.pool2.BasePooledObjectFactory
import org.apache.commons.pool2.PooledObject
import org.apache.commons.pool2.impl.DefaultPooledObject
import org.apache.commons.pool2.impl.SoftReferenceObjectPool
import org.eclipse.xtend.lib.annotations.Accessors
import org.slf4j.LoggerFactory
import cool.taomu.pool.SoftReferencePoolUtils.ObjectPoolWrapper
import org.apache.commons.pool2.PooledObjectFactory

@Accessors
class SoftReferencePoolFactory<T> extends BasePooledObjectFactory<ObjectPoolWrapper<T>> {
	val static LOG = LoggerFactory.getLogger(SoftReferencePoolFactory);
	var ClassLoader loader = null;
	var String name = null;
	var SoftReferenceObjectPool<ObjectPoolWrapper<T>> pool;
	var AtomicLong id = new AtomicLong(0);

	override create() throws Exception {
		if (!name.nullOrEmpty) {
			var zlass = loader.loadClass(this.name);
			var T instance = zlass.newInstance as T;
			return new ObjectPoolWrapper(this.pool, instance, this.id.incrementAndGet);
		}
	}

	override wrap(ObjectPoolWrapper<T> value) {
		return new DefaultPooledObject<ObjectPoolWrapper<T>>(value);
	}

	override PooledObject<ObjectPoolWrapper<T>> makeObject() throws Exception {
		LOG.info("创建对象")
		return super.makeObject();
	}

	override activateObject(PooledObject<ObjectPoolWrapper<T>> p) throws Exception {
		LOG.info("活动对象：" + " ==> " + p.object.id)
		super.activateObject(p);
	}

	override destroyObject(PooledObject<ObjectPoolWrapper<T>> p) throws Exception {
		LOG.info("销毁对象:" + " ==> " + p.object.id)
		super.destroyObject(p);
	}

	override passivateObject(PooledObject<ObjectPoolWrapper<T>> p) throws Exception {
		LOG.info("归还对象:" + " ==> " + p.object.id)
		super.passivateObject(p);
	}
}

class SoftReferencePoolUtils<T> {
	@Accessors
	static class ObjectPoolWrapper<T> implements AutoCloseable {
		long id;
		T instance;
		SoftReferenceObjectPool<ObjectPoolWrapper<T>> pool;

		new(SoftReferenceObjectPool<ObjectPoolWrapper<T>> pool, T instance, long id) {
			this.pool = pool;
			this.instance = instance;
			this.id = id;
		}

		override close() throws Exception {
			this.pool.returnObject(this)
		}
	}

	var SoftReferenceObjectPool<ObjectPoolWrapper<T>> pool;

	new(String name) {
		this(name,Thread.currentThread.contextClassLoader)
	}

	new(String name,PooledObjectFactory<ObjectPoolWrapper<T>> factory, ClassLoader loader) {
		this.pool = new SoftReferenceObjectPool<ObjectPoolWrapper<T>>(factory);
		if (factory instanceof SoftReferencePoolFactory) {
			factory.loader = loader;
			factory.pool = this.pool;
			factory.name = name;
		}
	}
	
	new(SoftReferencePoolFactory<ObjectPoolWrapper<T>> factory) {
		this(null,factory as PooledObjectFactory, Thread.currentThread.contextClassLoader);
	}

	new(String name,SoftReferencePoolFactory<ObjectPoolWrapper<T>> factory) {
		this(name,factory as PooledObjectFactory, Thread.currentThread.contextClassLoader);
	}

	new(String name,ClassLoader loader) {
		this(name,new SoftReferencePoolFactory<ObjectPoolWrapper<T>>() as PooledObjectFactory, loader);
	}

	def getInstance() {
		return this.pool.borrowObject();
	}

	def remove(SoftReferencePoolUtils.ObjectPoolWrapper<T> obj) {
		this.pool.invalidateObject(obj);
	}

	def close(SoftReferencePoolUtils.ObjectPoolWrapper<T> obj) {
		this.pool.returnObject(obj);
	}
}
