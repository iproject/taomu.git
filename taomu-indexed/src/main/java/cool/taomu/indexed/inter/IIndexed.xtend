/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.indexed.inter

/**
 * {keyPath: 'id'}
 * {unique: true|false}
 */
interface IIndexed {
	def void open(String name);	
	def void open(String name,String version);	
	def void createKeyPath(String id);
	def void createIndex(String fieldName,boolean unique);
	def Object get(String key);
	def void put(String result);
	def void remove(String key);
	def void clear();
}